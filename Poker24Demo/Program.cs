﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Poker24Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("    ***************************************************");
            Console.WriteLine("    *                   扑克牌算24                    *");
            Console.WriteLine("    *              Copyright 2021 凌风游              *");
            Console.WriteLine("    *     https://gitee.com/lkj1420428992/poker24     *");
            Console.WriteLine("    ***************************************************");
            Console.WriteLine();
            Console.WriteLine("    用+-*/运算，算出结果24。AJQK作为数字1");
            Console.WriteLine("    play:开始发牌; exit:退出; answer:查看答案");

            bool isExit = false;
            PlayPoker playPoker = new PlayPoker();
            while (!isExit) {
                string msg = Console.ReadLine();

                if (msg == "exit") {
                    isExit = true;
                    break;
                }
                else if (msg == "play") {
                    string title = playPoker.Play();
                    Console.WriteLine($"发牌：{title} 请写出公式：");
                    Console.WriteLine();
                    string formula = playPoker.InputFormula();
                    if (formula == "answer")
                    {
                        Console.WriteLine("正确答案：");
                        showAnswer(playPoker.PokerResult());
                        
                    }
                    else {
                        var res = playPoker.CheckFormula(formula);
                        if (res)
                        {
                            Console.WriteLine("回答正确");
                        }
                        else {
                            Console.WriteLine("回答错误");
                            Console.WriteLine("正确答案：");
                            showAnswer(playPoker.PokerResult());
                        }
                    }
                }
               
            }
        }

        static void showAnswer(List<string> answers) {

            if (answers == null || answers.Count == 0) return;
            answers.ForEach(item=> {
                Console.WriteLine(item);
            });
        }

    }
}
